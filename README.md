# コミケ用リポ

## 変換方法

htmlへ変換

```
$ pandoc -f markdown_github -t html5 circle_list.md -s -o circle_list.html
```

Wordファイルへ変換

```
$ pandoc -f markdown_github -t docx circle_list.md -s -o circle_list.docx
```
